import EquipmentsController from "../controllers/equipments";
export default app => {
  const equipmentsController = new EquipmentsController(
    app.datasource.models.Equipments
  );
  app
    .route("/equipments")
    .all(app.auth.authenticate())
    .get((req, res) => {
      equipmentsController.getAll().then(response => {
        res.status(response.statusCode);
        res.json(response.data);
      });
    })
    .post((req, res) => {
      equipmentsController.create(req.body).then(response => {
        res.status(response.statusCode);
        res.json(response.data);
      });
    });

  app
    .route("/equipments/:id")
    .all(app.auth.authenticate())
    .get((req, res) => {
      equipmentsController.getById(req.params).then(response => {
        res.status(response.statusCode);
        res.json(response.data);
      });
    })
    .put((req, res) => {
      equipmentsController.update(req.body, req.params).then(response => {
        res.status(response.statusCode);
        res.json(response.data);
      });
    })
    .delete((req, res) => {
      equipmentsController.delete(req.params).then(response => {
        res.sendStatus(response.statusCode);
      });
    });
};
