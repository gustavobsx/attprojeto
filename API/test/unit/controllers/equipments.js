import EquipmentsController from "../../../controllers/equipments";
describe("Controllers: Equipments", () => {
  describe("Get all equipments: getAll()", () => {
    it("should return a list of equipments", () => {
      const Equipments = {
        findAll: td.function()
      };
      const expectedResponse = [
        {
          id: 1,
          name: "testEquipment",
          created_at: "2018-06-25T23:55:36.692Z",
          updated_at: "2018-06-25T23:55:36.692Z"
        }
      ];
      td.when(Equipments.findAll({ })).thenResolve(expectedResponse);
      const equipmentsController = new EquipmentsController(Equipments);
      return equipmentsController
        .getAll()
        .then(response => expect(response.data).to.be.eql(expectedResponse));
    });
  });
});

describe("Get all equipments: getById()", () => {
  it("should return a equipments", () => {
    const Equipments = {
      findOne: td.function()
    };
    const expectedResponse = [
      {
        id: 1,
        name: "testEquipment",
        created_at: "2018-06-25T23:55:36.692Z",
        updated_at: "2018-06-25T23:55:36.692Z"
      }
    ];
    td.when(Equipments.findOne({where:{id:1}})).thenResolve(expectedResponse);
    const equipmentsController = new EquipmentsController(Equipments);
    return equipmentsController
      .getById({id:1})
      .then(response => expect(response.data).to.be.eql(expectedResponse));
  });
});

describe("Create a equipment: create()", () => {
  it("should create a equipment", () => {
    const Equipments = {
      create: td.function(),
    };
    const requestBody = {
      name: "testEquipment",
    };
    const expectedResponse = [
      {
        id: 1,
        name: "testEquipment",
        created_at: "2018-06-25T23:55:36.692Z",
        updated_at: "2018-06-25T23:55:36.692Z"
      }
    ];
    td.when(Equipments.create(requestBody)).thenResolve(expectedResponse);
    const equipmentsController = new EquipmentsController(Equipments);
    return equipmentsController
      .create(requestBody)
      .then(response => expect(response.data).to.be.eql(expectedResponse));
  });
});

describe("Update a equipment: update()", () => {
  it("should update an existing equipment", () => {
    const Equipments = {
      update: td.function(),
    };
    const requestBody = {
      id:1,
      name: "newEquipment",
    };
    const expectedResponse = {
        id: 1,
        name: "newEquipment",
        created_at: "2018-06-25T23:55:36.692Z",
        updated_at: "2018-06-25T23:55:36.692Z"
    };
    td.when(Equipments.update(requestBody, {where: {id: 1}} )).thenResolve(expectedResponse);
    const equipmentsController = new EquipmentsController(Equipments);
    return equipmentsController.update(requestBody, {id: 1} )
      .then(response => expect(response.data).to.be.eql(expectedResponse));
  });
});

describe("Delete a equipment: delete()", () => {
  it("should delete an existing equipment", () => {
    const Equipments = {
      destroy: td.function(),
    };

    td.when(Equipments.destroy({where: {id: 1}} )).thenResolve({});
    const equipmentsController = new EquipmentsController(Equipments);
    return equipmentsController.delete({id: 1})
      .then(response => expect(response.statusCode).to.be.eql(204));
  });
});

