describe("Routes Equipments", () => {
  const Equipments = app.datasource.models.Equipments;
  const defaultEquipment = {
    id: 1,
    descricao: "Default equipment",
    localizacao: "Sala B302",
    identificacao: "ANFGDD-3645",
    data_vencimento: "2018-12-12"
  };

  beforeEach(done => {
    Equipments.destroy({ where: {} })
      .then(() => Equipments.create(defaultEquipment))
      .then(() => {
        done();
      });
  });

  describe("Route GET /equipments", () => {
    it("should return a list of equipments", done => {
      const equipmentsList = Joi.array().items(
        Joi.object().keys({
          id: Joi.number(),
          descricao: Joi.string(),
          localizacao: Joi.string(),
          identificacao: Joi.string(),
          data_vencimento: Joi.date().iso(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso()
        }));
      request.get("/equipments").end((err, res) => {
        joiAssert(res.body, equipmentsList);
        done(err);
      });
    });
  });

  describe("Route GET /equipments/{id}", () => {
    it("should return a equipment", done => {
      const equipmentsList = Joi.array().items(
        Joi.object().keys({
          id: Joi.number(),
          descricao: Joi.string(),
          localizacao: Joi.string(),
          identificacao: Joi.string(),
          data_vencimento: Joi.date().iso(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso()
        })
      );
      request.get("/equipments/1").end((err, res) => {
        joiAssert(res.body, Equipments);
        done(err);
      });
    });
  });

  describe("Route POST /equipments", () => {
    it("should create a equipment", done => {
      const newEquipment = {
        id: 2,
        descricao: "Test Equipment",
        localizacao: "Sala RH-3",
        identificacao: "OSORINHO-LENHADOR",
        data_vencimento: "2018-06-25"
      };
      const equipmentsList = Joi.array().items(
        Joi.object().keys({
          id: Joi.number(),
          descricao: Joi.string(),
          localizacao: Joi.string(),
          identificacao: Joi.string(),
          data_vencimento: Joi.date().iso(),
          created_at: Joi.date().iso(),
          updated_at: Joi.date().iso()
        })
      );
      request
        .post("/equipments")
        .send(newEquipment)
        .end((err, res) => {
          joiAssert(res.body, Equipments);
          done(err);
        });
    });
  });

  describe("Route PUT /equipments/{id}", () => {
    it("should update a equipment", done => {
      const thirdEquipment = {
        id: 1,
        descricao: "Extintor para a Carlinha",
        localizacao: "Sala Oxygen-3",
        identificacao: "OSORINHO-LENHADOR",
        data_vencimento: "2018-06-25"
      };
      const updatedCount = joi.array().items(1);
      request
        .put("/equipments/1")
        .send(thirdEquipment)
        .end((err, res) => {
          joiAssert(res.body, updatedCount);
          done(err);
        });
    });
  });

  describe("Route DELETE /equipments/{id}", () => {
    it("should delete a equipment", done => {
      request.delete("/equipments/1").end((err, res) => {
        expect(res.statusCode).to.be.eql(204);
        done(err);
      });
    });
  });
});
