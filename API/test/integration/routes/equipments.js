import jwt from 'jwt-simple';
import HttpStatus from 'http-status';
describe("Routes Equipments", () => {
  const Equipments = app.datasource.models.Equipments;
  const Users = app.datasource.models.Users;
  const jwtSecret = app.config.jwtSecret;

  const defaultEquipment = {
    id: 1,
    descricao: "Default equipment",
    localizacao: "Sala B302",
    identificacao: "ANFGDD-3645",
    data_vencimento: "2018-12-12"
  };

  let token;

  beforeEach(done => {
    Users
    .destroy({ where: {} })
    .then(() => Users.create({
      name: 'Gustavo',
      email: 'gustavo@gustavo.com',
      password: '12345',
    }))
    .then(user => {
      Equipments
      .destroy({ where: {} })
      .then(() => Equipments.create(defaultEquipment))
      .then(() => {
        console.log("OIA O TOKENNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN");
        console.log();
        token = jwt.encode({ id: user.id }, jwtSecret);
        done();
      });
    });
  });

  describe("Route GET /equipments", () => {
    it("should return a list of equipments", done => {
      request.get("/equipments")
        .set('Authorization', `bearer ${token}`)
        .end((err, res) => {
          expect(res.body[0].id).to.be.eql(defaultEquipment.id);
          expect(res.body[0].descricao).to.be.eql(defaultEquipment.descricao);
          done(err);
        });
    });
  });

  describe("Route GET /equipments/{id}", () => {
    it("should return a equipment", done => {
      request.get("/equipments/1")
        .set('Authorization', `bearer ${token}`)
        .end((err, res) => {
          expect(res.body.id).to.be.eql(defaultEquipment.id);
          expect(res.body.descricao).to.be.eql(defaultEquipment.descricao);
          done(err);
        });
    });
  });

  describe("Route POST /equipments", () => {
    it("should create a equipment", done => {
      const newEquipment = {
        id: 2,
        descricao: "Test Equipment",
        localizacao: "Sala RH-3",
        identificacao: "OSORINHO-LENHADOR",
        data_vencimento: "2018-06-25"
      };
      request
        .post("/equipments")
        .set('Authorization', `bearer ${token}`)
        .send(newEquipment)
        .end((err, res) => {
          expect(res.body.id).to.be.eql(newEquipment.id);
          expect(res.body.descricao).to.be.eql(newEquipment.descricao);
          done(err);
        });
    });
  });

  describe("Route PUT /equipments/{id}", () => {
    it("should update a equipment", done => {
      const thirdEquipment = {
        id: 1,
        descricao: "Extintor para a Carlinha",
        localizacao: "Sala Oxygen-3",
        identificacao: "OSORINHO-LENHADOR",
        data_vencimento: "2018-06-25"
      };
      request
        .put("/equipments/1")
        .set('Authorization', `bearer ${token}`)
        .send(thirdEquipment)
        .end((err, res) => {
          expect(res.body).to.be.eql([1]);
          done(err);
        });
    });
  });

  describe("Route DELETE /equipments/{id}", () => {
    it("should delete a equipment", done => {
      request.delete("/equipments/1")
        .set('Authorization', `bearer ${token}`)
        .end((err, res) => {
          expect(res.statusCode).to.be.eql(HttpStatus.NO_CONTENT);
          done(err);
        });
    });
  });
});
