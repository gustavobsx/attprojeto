import Express from "express";
import config from "./config/config";
import datasource from "./config/datasource";
import bodyParser from "body-parser";
import equipmentsRouter from "./routes/equipments";
import usersRouter from "./routes/users";
import authorization from './auth';
import authRouter from './routes/auth';

const app = Express();
app.config = config;
app.datasource = datasource(app);

app.set("port", 7000);
app.use(bodyParser.json());
const auth = authorization(app);

app.use(auth.initialize());
app.auth = auth;

equipmentsRouter(app);
usersRouter(app);
authRouter(app);


export default app;
