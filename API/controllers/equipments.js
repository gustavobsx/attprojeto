import HttpStatus from 'http-status';
const defaultResponse = (data, statusCode = HttpStatus.OK) => ({
  data,
  statusCode
});
const errorResponse = (message, statusCode = HttpStatus.BAD_REQUEST) =>
  defaultResponse(
    {
      error: message
    },
    statusCode
  );

class EquipmentsController {
  constructor(Equipments) {
    this.Equipments = Equipments;
  }
  getAll() {
    return this.Equipments.findAll({})
      .then(result => defaultResponse(result))
      .catch(error => errorResponse(error.message));
  }
  getById(params) {
    return this.Equipments.findOne({
      where: params
    })
      .then(result => defaultResponse(result))
      .catch(error => errorResponse(error.message));
  }
  create(data) {
    return this.Equipments.create(data)
      .then(result => defaultResponse(result, HttpStatus.CREATED))
      .catch((error) => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  update(data, params) {
    return this.Equipments.update(data, {
      where: params
    })
      .then(result => defaultResponse(result))
      .catch((error) => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
  delete(params) {
    return this.Equipments.destroy({
      where: params
    })
      .then(result => defaultResponse(result, HttpStatus.NO_CONTENT))
      .catch((error) => errorResponse(error.message, HttpStatus.UNPROCESSABLE_ENTITY));
  }
}

export default EquipmentsController;
