export default (sequelize, DataType) => {
  const Equipments = sequelize.define("Equipments", {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    descricao: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    localizacao: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    identificacao: {
      type: DataType.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    data_vencimento: {
      type: DataType.DATE,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  });
  return Equipments;
};
