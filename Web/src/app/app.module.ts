import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { MaterializeModule } from "angular2-materialize";
import { AutenticaService } from './login/autentica.service';
import { MenuComponent } from './shared/menu/menu.component';
import { UserComponent } from "./forms/user/add-user/user.component";
import { AreaComponent } from './forms/area/area.component';
import { UnityComponent } from './forms/unity/unity.component';
import { HomeComponent } from './shared/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { PerfilComponent } from "./forms/user/perfil/perfil.component";
import { NFoundComponent } from './shared/not-found/n-found.component';
import { ResetpasswordComponent } from './shared/resetpassword/resetpassword.component';
import { AddEquipComponent } from './forms/equip/add-equip/add-equip.component';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MenuComponent,
    UserComponent,
    AreaComponent,
    UnityComponent,
    HomeComponent,
    PerfilComponent,
    NFoundComponent,
    ResetpasswordComponent,
    AddEquipComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    NgbModule.forRoot()

  ],
  providers: [AutenticaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
