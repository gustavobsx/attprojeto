import { NgModule } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-user-form',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
@NgModule({
  imports:      [
    CommonModule,
    FormGroup,
    ReactiveFormsModule
  ],
  declarations: [],
  exports: [
    CommonModule,
    FormsModule,
    FormGroup,
    ReactiveFormsModule
  ]
})
export class UserComponent implements OnInit {
  constructor() { }

  ngOnInit() {
  }
  onSubmit(){
    alert("Funcionando rapazzz");
  }

}
