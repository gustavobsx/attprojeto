import { HomeComponent } from "./shared/home/home.component";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { importType } from "@angular/compiler/src/output/output_ast";
import { LoginComponent } from "./login/login.component";
import { UserComponent } from "./forms/user/add-user/user.component";
import { UnityComponent } from "./forms/unity/unity.component";
import { AreaComponent } from "./forms/area/area.component";
import { PerfilComponent } from "./forms/user/perfil/perfil.component";
import { NFoundComponent } from "./shared/not-found/n-found.component";
import { ResetpasswordComponent } from "./shared/resetpassword/resetpassword.component";
import { AddEquipComponent } from "./forms/equip/add-equip/add-equip.component";

const routes: Routes = [
  { path: "", component: LoginComponent },
  { path: "nf", component: NFoundComponent },
  { path: "reset", component: ResetpasswordComponent },
  {
    path: "sesi",
    component: HomeComponent,
    children: [
      { path: "user", component: UserComponent },
      { path: "add-unity", component: UnityComponent },
      { path: "add-area", component: AreaComponent },
      { path: "add-equip", component: AddEquipComponent },
      { path: "perfil", component: PerfilComponent }
    ]
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule {}
