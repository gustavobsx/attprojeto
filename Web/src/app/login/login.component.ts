import { Usuario } from './usuario';
import { AutenticaService } from './autentica.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private usuario: Usuario = new Usuario();

  constructor (private authService: AutenticaService) {}

  
  ngOnInit() {
 
  }

  fazerLogin(){
   this.authService.fazerLogin(this.usuario);
   }
   
}