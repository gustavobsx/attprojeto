import { TestBed, inject } from '@angular/core/testing';

import { AutenticaService } from './autentica.service';

describe('AutenticaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AutenticaService]
    });
  });

  it('should be created', inject([AutenticaService], (service: AutenticaService) => {
    expect(service).toBeTruthy();
  }));
});
